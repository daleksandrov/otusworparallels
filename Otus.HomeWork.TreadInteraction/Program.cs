﻿using System;
using System.Diagnostics;

namespace Otus.HomeWork.TreadInteraction
{
    class Program
    {
        static void Main(string[] args)
        {
            CalcArraySum calcArraySum = new CalcArraySum(3_000_000);

            TimeTest(() =>
            {
                Console.Write($"Последовательное вычисление: {calcArraySum.Sum()} ");
            });
            
            TimeTest(() =>
            {
                Console.Write($"Параллельное вычисление (Threads): {calcArraySum.SumByThreads(5)} ");
            });
            
            TimeTest(() =>
            {
                Console.Write($"Параллельное вычисление (Linq): {calcArraySum.SumByParallelLinq()} ");
            });
            
            TimeTest(() =>
            {
                Console.Write($"Параллельное вычисление (Parallel.For): {calcArraySum.SumByParallelFor(5)} ");
            });
        }
        
        private static void TimeTest(Action act)
        {
            var timer = Stopwatch.StartNew();
            act();
            timer.Stop();
            Console.WriteLine($"Время: {timer.ElapsedMilliseconds} мс.");
        }
    }
}