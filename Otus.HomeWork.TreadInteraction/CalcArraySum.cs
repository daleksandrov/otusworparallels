﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.HomeWork.TreadInteraction
{
    public class CalcArraySum
    {
        public static Random _random = new Random();
        private int[] _array;

        public CalcArraySum(int count)
        {
            _array = GenerateArray(count);
        }

        private int[] GenerateArray(int count)
        {
            var arr = new int[count];
            for (var i = 0; i < count; i++)
                arr[i] = _random.Next(10);
            return arr;
        }

        /// <summary>
        /// Обычное последовательное вычисление суммы
        /// </summary>
        /// <returns></returns>
        public int Sum()
        {
            int sum = 0;

            for (int i = 0; i < _array.Length; i++)
            {
                sum += _array[i];
            }

            return sum;
        }

        /// <summary>
        /// Параллельное вычисление суммы с применением Thread
        /// </summary>
        /// <param name="countThreads"></param>
        /// <returns></returns>
        public int SumByThreads(int countThreads)
        {
            Thread[] threads = new Thread[countThreads];
            int[] sums = new int[countThreads];
            
            //Размер массива для потока
            int threadArrSize = _array.Length / countThreads;

            for (int i = 0; i < countThreads; i++)
            {
                int localOffset = i;
                Thread thread = new Thread(() =>
                {
                    int sum = 0;
                    for (int j = localOffset * threadArrSize; j < (localOffset + 1) * threadArrSize; j++)
                    {
                        sum += _array[j];
                    }

                    sums[localOffset] = sum;
                });

                thread.Start();
                
                threads[localOffset] = thread;
            }
            

            //Дожидаемся выполнения всех потоков и подсчитываем итоговую сумму
            int totalSum = 0;
            for (int i = 0; i < threads.Length; i++)
            {
                threads[i].Join();
                totalSum += sums[i];
            }

            return totalSum;
        }
        
        /// <summary>
        /// Параллельное вычисление суммы с применением Linq
        /// AsParallel().Sum()
        /// </summary>
        /// <param name="countThreads"></param>
        /// <returns></returns>
        public int SumByParallelLinq()
        {
            return _array.AsParallel().Sum();
        }
        
        /// <summary>
        /// Параллельное вычисление суммы с применением Parallel.For
        /// 
        /// </summary>
        /// <param name="countThreads"></param>
        /// <returns></returns>
        public int SumByParallelFor(int countThreads)
        {
            int[] sums = new int[countThreads];
            
            //Размер массива для потока
            int threadArrSize = _array.Length / countThreads;

            Parallel.For(0, countThreads, localOffset =>
            {
                int sum = 0;
                for (int j = localOffset * threadArrSize; j < (localOffset + 1) * threadArrSize; j++)
                {
                    sum += _array[j];
                }

                sums[localOffset] = sum;
            });
            
            int totalSum = 0;
            for (int i = 0; i < countThreads; i++)
            {
                totalSum += sums[i];
            }

            return totalSum;
        }
    }
}